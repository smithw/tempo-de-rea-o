Para instalar e utilizar esse software, é necessário possuir no seu
computador o sistema operacional Linux, a versão de desenvolvimento da
biblioteca Qt 4.8.3 e um compilador de C++ capaz de utilizar os recursos
do padrão C++11 (ISO/IEC 14882:2011), como o GCC 4.7 ou superior ou
Clang/LLVM 3.1 ou superior.

# Download

Para baixar o código-fonte deste programa, você pode optar por clonar o
repositório git ou baixar o arquivo zipado diretamente do BitBucket. É
altamente recomendável clonar o repositório, pois você poderá obter
atualizações do software utilizando unicamente o comando `git pull`.

## Repositório GIT

Para fazer download por meio do repositório git, basta digitar, na linha
de comando, a seguinte instrução:

	$ git clone https://smithw@bitbucket.org/smithw/tempo-de-rea-o.git

Certifique-se de estar no diretório em que você quer baixar os arquivos
primeiramente. **É necessário possuir o utilitário git instalado para
utilização deste método**.

## Download do ZIP

Para baixar o arquivo ZIP contendo todos os arquivos necessários, basta
dirigir-se à aba *Overview* do endereço
http://git.alignleft.net/tempo-de-rea-o e, na tabela de informações
localizada no lado direito da tela, clica no link "Download", localizado
ao lado da informação "Size".

# Preparação

Para compilar, é necessário possuir as bibliotecas de desenvolvimento do
framework Qt, versão 4.8.3 ou superior. Na distribuição Ubuntu, os pacotes
necessários são:

* libqt4-dev
* libqt4-dev-bin
* libqt4-sql
* qt4-dev-tools
* qt4-qmake

Todos esses pacotes podem ser instalados sem a adição de nenhum
repositório adicional ao Ubuntu, utilizando-se o seguinte comando:

	$ sudo apt-get install libqt4-dev libqt4-dev-bin libqt4-sql qt4-dev-tools qt4-qmake

Além disso, é necessário possuir um compilador compatível com o padrão
C++11. Recomenda-se a utilização do GCC versão 4.7 ou superior, embora seja
possível utilizar o Clang/LLVM versão 3.1 ou superior (O programa Tempo
de Reação se utiliza do módulo `chrono` do C++11, e o Clang/LLVM possui
uma incompatibilidade com a versão do `chrono` incluída na `libstdc++`
do GCC; é necessário, portanto, no caso da utilização Clang/LLVM,
instalar a biblioteca `libc++` provida pelo mesmo, o que pode se revelar
um grande empecilho).

Para instalar os pacotes que provêm o GCC 4.7, na distribuição Ubuntu,
pode ser utilizado o seguinte comando:

	$ sudo apt-get install build-essential g++-4.7 libstdc++6-4.7-dev make

# Compilação

Para dar início ao processo de compilação, entre no diretório raiz
do código-fonte e digite:

	$ qmake CONFIG+=release
	$ make

O programa `tempo_reacao` estará disponível, após a compilação, no
subdiretório `build`.

# Utilização

À medida que testes forem sendo realizados, um arquivo será criado na
mesma pasta em que se encontra o executável do programa, com o nome
`results.sqlite`. Para verificar os resultados, é necessário um cliente
de sqlite, como `sqlite3` (linha de comando) ou sqliteman (interface
gráfica).

Ambos podem ser instalados na distribuição Ubuntu com o seguinte
comando:

	$ sudo apt-get install sqlite3 sqliteman

O sqliteman integra-se automaticamente ao gerenciador de arquivos do
Ubuntu e abre qualquer arquivo de banco de dados sqlite que for
clicado duas vezes.
