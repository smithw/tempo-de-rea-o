#ifndef TIMER_H
#define TIMER_H 

#include <chrono>
#include <thread>
#include <functional>
#include <memory>

typedef std::chrono::time_point<std::chrono::high_resolution_clock> hr_time_point;

class Timer {
public:
	typedef std::function<void (const Timer&)> signal_callback;
	typedef std::function<void (Timer&)> start_callback;
	typedef std::function<void (const Timer&, std::chrono::microseconds)> stop_callback;

private:
	signal_callback signal_wait;
	signal_callback signal_press;
	std::chrono::milliseconds duration;
	std::unique_ptr<hr_time_point> startTime;


public:
	static const signal_callback sig_noop;
	static const start_callback start_noop;
	static const stop_callback stop_noop;

	Timer();
	Timer(signal_callback, signal_callback);
	Timer(signal_callback, signal_callback, std::pair<int, int>);

	void coldStart(start_callback, start_callback);
	void setWaitTime(std::pair<int, int> range);
	std::chrono::milliseconds getWaitTime();
	void start(start_callback, start_callback);
	void start();
	void stop(stop_callback);
	void stop();
};

#endif