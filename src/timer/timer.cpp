#include "timer.h"
#include <cstdlib>

extern Timer::signal_callback const Timer::sig_noop = [] (const Timer& t) {};
extern Timer::start_callback const Timer::start_noop = [] (Timer& t) {};
extern Timer::stop_callback const Timer::stop_noop = [] (const Timer& t, std::chrono::microseconds d) {};

void Timer::setWaitTime(std::pair<int, int> range) {
	int randTime = range.first + (std::rand() % (range.second - range.first));
	duration = std::chrono::milliseconds(randTime);
}

Timer::Timer() {
	signal_wait = sig_noop;
	signal_press = sig_noop;
	setWaitTime(std::make_pair(2000, 4000));
}

Timer::Timer(Timer::signal_callback signal_wait, Timer::signal_callback signal_press)
: signal_wait {signal_wait}, signal_press {signal_press} {
	setWaitTime(std::make_pair(2000, 4000));
}

Timer::Timer(Timer::signal_callback signal_wait, Timer::signal_callback signal_press, std::pair<int, int> range)
: signal_wait {signal_wait}, signal_press {signal_press} {
	setWaitTime(range);
}

void Timer::start(Timer::start_callback before_start, Timer::start_callback after_start) {
	using namespace std;

	signal_wait(*this);
	this_thread::sleep_for(duration);

	coldStart(before_start, after_start);
}

void Timer::coldStart(Timer::start_callback before_start, Timer::start_callback after_start) {
	using namespace std;
	signal_press(*this);

	before_start(*this);
	startTime = unique_ptr<hr_time_point>(new hr_time_point(chrono::high_resolution_clock::now()));
	after_start(*this);
}

std::chrono::milliseconds Timer::getWaitTime() {
	return duration;
}

void Timer::start() {
	start(start_noop, start_noop);
}

void Timer::stop(Timer::stop_callback after_stop) {
	using namespace std;
	chrono::microseconds elapsed;
	auto endTime = chrono::high_resolution_clock::now();

	if (startTime) {
		auto sTime = *startTime.release();
		elapsed = chrono::duration_cast<chrono::microseconds>(endTime - sTime);
	}
	else elapsed = chrono::microseconds(0);

	after_stop(*this, elapsed);
}

void Timer::stop() {
	stop(stop_noop);
}