#include "BaseMixed.h"
#include <algorithm>
#include <random>

using namespace Tests;

void BaseMixed::shufflePainters() {
	generatePainters();

	std::random_device rd;
	std::mt19937 g(rd());

	std::shuffle(painters.begin(), painters.end(), g);
	setNumberOfTests(painters.size());
}

bool BaseMixed::shouldStoreResult() {
	return shouldStore;
}

void BaseMixed::paintPress(const Timer& t) {
	int i = getTestsCompleted();

	painters[i]();
}