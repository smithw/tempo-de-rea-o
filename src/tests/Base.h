#ifndef TEST_INTERFACE_H
#define TEST_INTERFACE_H

#include <QtCore>
#include <QtGui>
#include <memory>
#include <vector>
#include <chrono>
#include "../ui/TestDialog.h"
#include "../timer/timer.h"

namespace Tests {
	class Base : public QObject {
		Q_OBJECT
	private:
		std::unique_ptr<Timer> timer;
		std::pair<int, int> waitRange;
		unsigned numberOfTests;
		bool isTestRunning;
		bool canCapture;

	public:
		Base(std::weak_ptr<TestDialog> testDialog);
		virtual ~Base() { }

		virtual QString testIdentifier() = 0;

		virtual void paintWait(const Timer&);
		virtual void paintPress(const Timer&) = 0;
		virtual void allowResponse(Timer&);
		virtual void disallowResponse();
		virtual void allowCapture(Timer&);
		virtual void storeResponse(const Timer&, std::chrono::microseconds);

		unsigned getNumberOfTests();
		void setNumberOfTests(unsigned);
		unsigned getTestsCompleted();
		std::pair<int, int> getWaitRange();
		void setWaitRange(std::pair<int, int>);
		bool getIsTestRunning();

		void execute();

	protected:
		std::weak_ptr<TestDialog> testDialog;
		std::vector<std::chrono::microseconds> results;
		QString instructions;
		unsigned testsCompleted;

		Timer::signal_callback _paintWait;
		Timer::signal_callback _paintPress;
		Timer::start_callback _allowResponse;
		Timer::start_callback _allowCapture;
		Timer::stop_callback _storeResponse;

		virtual void runSingleTest();

		virtual bool shouldStoreResult();
		QString generateInstructions(std::vector<std::string>&& instructionList);

	public slots:
		virtual void responseCaptured();
		void coldStart();

	signals:
		void testStarted(const QString& testName);
		void testCompleted(const std::vector<std::chrono::microseconds>& results);
		void dataPointAcquired(std::chrono::microseconds result);
		void testCanceled();

	};

}


#endif