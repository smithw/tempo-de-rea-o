#ifndef TEMPODEREACAO_BASE_MIXED_H
#define TEMPODEREACAO_BASE_MIXED_H

#include "Base.h"

namespace Tests {
	class BaseMixed : public Base {
	public:
		BaseMixed(std::weak_ptr<TestDialog> dialog) : Base(dialog) { }
		virtual void paintPress(const Timer&);

	protected:
		typedef std::function<void ()> Painter;
		std::vector<Painter> painters;
		bool shouldStore;

		void shufflePainters();
		virtual bool shouldStoreResult();
		virtual void generatePainters() = 0;
	};
}


#endif