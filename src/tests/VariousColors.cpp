#include "VariousColors.h"

using namespace Tests;

VariousColors::VariousColors(std::weak_ptr<TestDialog> dialog) : BaseMixed(dialog) {
	instructions = generateInstructions(std::vector<std::string> {
		"Aperte qualquer tecla quando o <b>círculo vermelho</b> aparecer.",
		"<b>Não</b> pressione nada quando outras cores aparecem."
	});

	shufflePainters();
}

QString VariousColors::testIdentifier() {
	return "VariasCores";
}

void VariousColors::generatePainters() {
	painters = std::vector<Painter>(4, [this] () {
		if (auto dialog = testDialog.lock()) {
			dialog->drawCircle(Qt::red);
			shouldStore = true;
		}
	});

	painters.insert(painters.end(), 2, [this] () {
		if (auto dialog = testDialog.lock()) {
			dialog->drawCircle(Qt::yellow);
			shouldStore = false;
			QTimer::singleShot(2000, this, SLOT(responseCaptured()) );
		}
	});

	painters.insert(painters.end(), 2, [this] () {
		if (auto dialog = testDialog.lock()) {
			dialog->drawCircle(Qt::darkGreen);
			shouldStore = false;
			QTimer::singleShot(2000, this, SLOT(responseCaptured()) );
		}
	});

	painters.insert(painters.end(), 2, [this] () {
		if (auto dialog = testDialog.lock()) {
			dialog->drawCircle(Qt::blue);
			shouldStore = false;
			QTimer::singleShot(2000, this, SLOT(responseCaptured()) );
		}
	});
}
