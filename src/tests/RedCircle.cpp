#include "RedCircle.h"

using namespace Tests;

RedCircle::RedCircle(std::weak_ptr<TestDialog> testDialog) : Base(testDialog) {
	instructions = generateInstructions(std::vector<std::string> {
		"Aperte qualquer tecla quando o círculo vermelho aparecer."
	});

}

QString RedCircle::testIdentifier() {
	return "CirculoVermelho";
}

void RedCircle::paintPress(const Timer& t) {
	if (auto dialog = testDialog.lock()) {
		dialog->drawCircle(Qt::red);
	}
}
