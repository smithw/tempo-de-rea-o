#include "VariousMixed.h"

using namespace Tests;

VariousMixed::VariousMixed(std::weak_ptr<TestDialog> dialog) : BaseMixed(dialog) {
	instructions = generateInstructions(std::vector<std::string> {
		"Aperte qualquer tecla quando o <b>círculo vermelho</b> aparecer.",
		"<b>Não</b> pressione nada quando outras cores ou formatos aparecem."
	});

	shufflePainters();
}

QString VariousMixed::testIdentifier() {
	return "VariosMisturado";
}

VariousMixed::Painter VariousMixed::genPainter(Qt::GlobalColor color, Format f, bool store) {
	return [this, color, f, store] () {
		if (auto dialog = testDialog.lock()) {
			switch (f) {
				case Format::kCircle:
					dialog->drawCircle(color); break;
				case Format::kSquare:
					dialog->drawSquare(color); break;
				case Format::kTriangle:
					dialog->drawTriangle(color); break;
				default:
					dialog->drawDiamond(color);
			}

			shouldStore = store;
			if (!store) QTimer::singleShot(2000, this, SLOT(responseCaptured()) );
		}
	};
}

void VariousMixed::generatePainters() {
	painters.insert(painters.end(), 5, genPainter(Qt::red, Format::kCircle, true));

	painters.insert(painters.end(), 1, genPainter(Qt::yellow, Format::kCircle, false));
	painters.insert(painters.end(), 1, genPainter(Qt::blue, Format::kCircle, false));
	painters.insert(painters.end(), 1, genPainter(Qt::darkGreen, Format::kCircle, false));

	painters.insert(painters.end(), 1, genPainter(Qt::red, Format::kSquare, false));
	painters.insert(painters.end(), 1, genPainter(Qt::yellow, Format::kSquare, false));
	painters.insert(painters.end(), 1, genPainter(Qt::blue, Format::kSquare, false));
	painters.insert(painters.end(), 1, genPainter(Qt::darkGreen, Format::kSquare, false));

	painters.insert(painters.end(), 1, genPainter(Qt::red, Format::kDiamond, false));
	painters.insert(painters.end(), 1, genPainter(Qt::yellow, Format::kDiamond, false));
	painters.insert(painters.end(), 1, genPainter(Qt::blue, Format::kDiamond, false));
	painters.insert(painters.end(), 1, genPainter(Qt::darkGreen, Format::kDiamond, false));

	painters.insert(painters.end(), 1, genPainter(Qt::red, Format::kTriangle, false));
	painters.insert(painters.end(), 1, genPainter(Qt::yellow, Format::kTriangle, false));
	painters.insert(painters.end(), 1, genPainter(Qt::blue, Format::kTriangle, false));
	painters.insert(painters.end(), 1, genPainter(Qt::darkGreen, Format::kTriangle, false));


}
