#ifndef TEMPODEREACAO_VARIOUS_COLORS_H
#define TEMPODEREACAO_VARIOUS_COLORS_H

#include "BaseMixed.h"

namespace Tests {
	class VariousColors : public BaseMixed {

	public:
		VariousColors(std::weak_ptr<TestDialog>);
		virtual QString testIdentifier();

	protected:
		virtual void generatePainters();
	};
}

#endif