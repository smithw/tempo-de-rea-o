#include "RedTriangle.h"

using namespace Tests;

RedTriangle::RedTriangle(std::weak_ptr<TestDialog> testDialog) : Base(testDialog) {
	instructions = generateInstructions(std::vector<std::string> {
		"Aperte qualquer tecla quando o triângulo vermelho aparecer."
	});

}

QString RedTriangle::testIdentifier() {
	return "TrianguloVermelho";
}

void RedTriangle::paintPress(const Timer& t) {
	if (auto dialog = testDialog.lock()) {
		dialog->drawTriangle(Qt::red);
	}
}
