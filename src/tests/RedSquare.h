#ifndef REDSQUARE_H
#define REDSQUARE_H

#include "Base.h"
#include <QtGui>
#include <QtCore>
#include <memory>
#include "../ui/TestDialog.h"

namespace Tests {
	class RedSquare : public Base {
	public:
		RedSquare(std::weak_ptr<TestDialog>);
		virtual QString testIdentifier();
		virtual void paintPress(const Timer&);
	};
}

#endif
