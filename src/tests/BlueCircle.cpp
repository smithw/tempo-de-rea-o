#include "BlueCircle.h"

using namespace Tests;

BlueCircle::BlueCircle(std::weak_ptr<TestDialog> testDialog) : Base(testDialog) {
	instructions = generateInstructions(std::vector<std::string> {
		"Aperte qualquer tecla quando o círculo azul aparecer."
	});

}

QString BlueCircle::testIdentifier() {
	return "CirculoAzul";
}

void BlueCircle::paintPress(const Timer& t) {
	if (auto dialog = testDialog.lock()) {
		dialog->drawCircle(Qt::blue);
	}
}