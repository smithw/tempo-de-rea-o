#include "VariousFormats.h"

using namespace Tests;

VariousFormats::VariousFormats(std::weak_ptr<TestDialog> dialog) : BaseMixed(dialog) {
	instructions = generateInstructions(std::vector<std::string> {
		"Aperte qualquer tecla quando o <b>círculo vermelho</b> aparecer.",
		"<b>Não</b> pressione nada quando outras formas aparecem."
	});

	shufflePainters();
}

QString VariousFormats::testIdentifier() {
	return "VariosFormatos";
}

void VariousFormats::generatePainters() {
	painters = std::vector<Painter>(4, [this] () {
		if (auto dialog = testDialog.lock()) {
			dialog->drawCircle(Qt::red);
			shouldStore = true;
		}
	});

	painters.insert(painters.end(), 2, [this] () {
		if (auto dialog = testDialog.lock()) {
			dialog->drawSquare(Qt::red);
			shouldStore = false;
			QTimer::singleShot(2000, this, SLOT(responseCaptured()) );
		}
	});

	painters.insert(painters.end(), 2, [this] () {
		if (auto dialog = testDialog.lock()) {
			dialog->drawDiamond(Qt::red);
			shouldStore = false;
			QTimer::singleShot(2000, this, SLOT(responseCaptured()) );
		}
	});

	painters.insert(painters.end(), 2, [this] () {
		if (auto dialog = testDialog.lock()) {
			dialog->drawTriangle(Qt::red);
			shouldStore = false;
			QTimer::singleShot(2000, this, SLOT(responseCaptured()) );
		}
	});
}
