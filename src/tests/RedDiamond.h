#ifndef REDDIAMOND_H
#define REDDIAMOND_H

#include "Base.h"
#include <QtGui>
#include <QtCore>
#include <memory>
#include "../ui/TestDialog.h"

namespace Tests {
	class RedDiamond : public Base {
	public:
		RedDiamond(std::weak_ptr<TestDialog>);
		virtual QString testIdentifier();
		virtual void paintPress(const Timer&);
	};
}

#endif
