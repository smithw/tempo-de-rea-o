#ifndef REDTRIANGLE_H
#define REDTRIANGLE_H

#include "Base.h"
#include <QtGui>
#include <QtCore>
#include <memory>
#include "../ui/TestDialog.h"

namespace Tests {
	class RedTriangle : public Base {
	public:
		RedTriangle(std::weak_ptr<TestDialog>);
		virtual QString testIdentifier();
		virtual void paintPress(const Timer&);
	};
}

#endif
