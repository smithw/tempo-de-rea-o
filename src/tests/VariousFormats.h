#ifndef TEMPODEREACAO_VARIOUS_FORMATS_H
#define TEMPODEREACAO_VARIOUS_FORMATS_H

#include "BaseMixed.h"

namespace Tests {
	class VariousFormats : public BaseMixed {

	public:
		VariousFormats(std::weak_ptr<TestDialog>);
		virtual QString testIdentifier();

	protected:
		virtual void generatePainters();
	};
}

#endif