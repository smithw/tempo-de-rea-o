#ifndef REDCIRCLE_H
#define REDCIRCLE_H

#include "Base.h"
#include <QtGui>
#include <QtCore>
#include <memory>
#include "../ui/TestDialog.h"

namespace Tests {
	class RedCircle : public Base {
	public:
		RedCircle(std::weak_ptr<TestDialog>);
		virtual QString testIdentifier();
		virtual void paintPress(const Timer&);
	};
}

#endif
