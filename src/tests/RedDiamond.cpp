#include "RedDiamond.h"

using namespace Tests;

RedDiamond::RedDiamond(std::weak_ptr<TestDialog> testDialog) : Base(testDialog) {
	instructions = generateInstructions(std::vector<std::string> {
		"Aperte qualquer tecla quando o losango vermelho aparecer."
	});

}

QString RedDiamond::testIdentifier() {
	return "LosangoVermelho";
}

void RedDiamond::paintPress(const Timer& t) {
	if (auto dialog = testDialog.lock()) {
		dialog->drawDiamond(Qt::red);
	}
}
