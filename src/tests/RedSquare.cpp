#include "RedSquare.h"

using namespace Tests;

RedSquare::RedSquare(std::weak_ptr<TestDialog> testDialog) : Base(testDialog) {
	instructions = generateInstructions(std::vector<std::string> {
		"Aperte qualquer tecla quando o quadrado vermelho aparecer."
	});

}

QString RedSquare::testIdentifier() {
	return "QuadradoVermelho";
}

void RedSquare::paintPress(const Timer& t) {
	if (auto dialog = testDialog.lock()) {
		dialog->drawSquare(Qt::red);
	}
}
