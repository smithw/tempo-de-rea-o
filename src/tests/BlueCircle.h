#ifndef BLUECIRCLE_H
#define BLUECIRCLE_H

#include "Base.h"
#include <QtGui>
#include <QtCore>
#include <memory>
#include "../ui/TestDialog.h"

namespace Tests {
	class BlueCircle : public Base {
	public:
		BlueCircle(std::weak_ptr<TestDialog>);
		virtual QString testIdentifier();
		virtual void paintPress(const Timer&);
	};
}

#endif