#ifndef TEMPODEREACAO_VARIOUS_MIXED_H
#define TEMPODEREACAO_VARIOUS_MIXED_H

#include "BaseMixed.h"

enum class Format { kCircle, kSquare, kDiamond, kTriangle };

namespace Tests {
	class VariousMixed : public BaseMixed {
	private:
		Painter genPainter(Qt::GlobalColor, Format, bool);

	public:
		VariousMixed(std::weak_ptr<TestDialog>);
		virtual QString testIdentifier();

	protected:
		virtual void generatePainters();
	};
}

#endif