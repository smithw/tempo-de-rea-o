#include "Base.h"
#include <utility>

#include "../ui/InstructionsDialog.h"
#include "../utils/utils.h"

using namespace Tests;

Base::Base(std::weak_ptr<TestDialog> testDialog) : QObject(nullptr), testDialog(testDialog) {
	_paintWait = [this] (const Timer& t) { paintWait(t); };
	_paintPress = [this] (const Timer& t) { paintPress(t); };
	_allowResponse = [this] (Timer& t) { allowResponse(t); };
	_allowCapture = [this] (Timer& t) { allowCapture(t); };
	_storeResponse = [this] (const Timer& t, std::chrono::microseconds r) { storeResponse(t, r); };

	timer = std::unique_ptr<Timer>(nullptr);
	waitRange = std::make_pair(2000, 4000);
	numberOfTests = 10;
	testsCompleted = 0;
	isTestRunning = false;
	canCapture = false;
}

void Base::paintWait(const Timer& t) {
	if (auto sharedDialog = testDialog.lock()) {
		sharedDialog->drawText("Espere...");
	}
}

void Base::allowResponse(Timer& t) {
	if (auto sharedDialog = testDialog.lock()) {
		if (shouldStoreResult()) connect(
			sharedDialog.get(), SIGNAL(keyPressed()),
			this, SLOT(responseCaptured()) );
	}
}

void Base::disallowResponse() {
	if (auto sharedDialog = testDialog.lock()) {
		if (shouldStoreResult()) disconnect(
			sharedDialog.get(), SIGNAL(keyPressed()),
			this, SLOT(responseCaptured()) );
	}
}

void Base::allowCapture(Timer& t) {
	canCapture = true;
}

void Base::storeResponse(const Timer& t, std::chrono::microseconds result) {
	canCapture = false;
	disallowResponse();

	testsCompleted++;

	if (shouldStoreResult()) {
		results.push_back(result);
		emit dataPointAcquired(result);
	}

	runSingleTest();
}

unsigned Base::getNumberOfTests() {
	return numberOfTests;
}

void Base::setNumberOfTests(unsigned newNumberOfTests) {
	numberOfTests = newNumberOfTests;
}

unsigned Base::getTestsCompleted() {
	return testsCompleted;
}

std::pair<int, int> Base::getWaitRange() {
	return waitRange;
}

void Base::setWaitRange(std::pair<int, int> newRange) {
	waitRange = newRange;
}

bool Base::getIsTestRunning() {
	return isTestRunning;
}

void Base::execute() {
	if (auto dialog = testDialog.lock()) {
		auto inst = utils::make_unique<InstructionsDialog>(dialog.get());

		inst->setWindowModality(Qt::ApplicationModal);
		inst->setInstructionsText(std::move(instructions));

		if (inst->exec() == QDialog::Accepted) {
			timer = utils::make_unique<Timer>(_paintWait, _paintPress, waitRange);

			isTestRunning = true;
			emit testStarted(testIdentifier());
			runSingleTest();
		}
		else {
			emit testCanceled();
		}

	}

}

void Base::runSingleTest() {
	if (testsCompleted < numberOfTests) {
		timer->setWaitTime(waitRange);
		auto waitTime = timer->getWaitTime();

		paintWait(*timer);
		QTimer::singleShot(waitTime.count(), this, SLOT(coldStart()));
	}
	else {
		isTestRunning = false;
		timer.release();
		emit testCompleted(results);
	}
}

void Base::coldStart() {
	timer->coldStart(_allowResponse, _allowCapture);
}

void Base::responseCaptured() {
	if (isTestRunning && canCapture) timer->stop(_storeResponse);
}

bool Base::shouldStoreResult() {
	return true;
}

QString Base::generateInstructions(std::vector<std::string>&& instructionList) {
	QString inst = QString::fromUtf8(u8R"+(<h1>Instruções</h1>
	<p><ul>)+");

	for (std::string s : instructionList) {
		inst += "<li>" + QString::fromUtf8(s.c_str()) + "</li>";
	}

	inst += QString::fromUtf8(u8R"+(<li>Para cancelar o teste, pressione ESC ou clique no botão abaixo da área de teste.</li>
		<li><b>ATENÇÃO</b>: Se você cancelar o teste, <b>TODOS</b> os dados capturados serão perdidos!</li>
	</ul></p>)+");

	return inst;
}
