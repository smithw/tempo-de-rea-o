#include "AppController.h"
#include "../model/Result.h"
#include <cstdlib>
#include <iostream>

AppController::AppController(int argc, char *argv[]) : QObject(nullptr), app(argc, argv) {
	infoDialog = std::make_shared<InfoDialog>();
	QTimer::singleShot(0, this, SIGNAL(appStarting()));

	connectSignals();
	generateTests();
}

void AppController::connectSignals() {
	if (infoDialog) {
		connect(
			infoDialog.get(), SIGNAL(rejected()),
			&app, SLOT(quit()) );

		connect(
			infoDialog.get(), SIGNAL(accepted()),
			this, SLOT(startTesting()) );
	}
	else qDebug() << "infoDialog is somehow empty.";

	connect(
		this, SIGNAL(appStarting()),
		this, SLOT(initializeDb()) );
}

void AppController::generateTests() {
	std::vector<TestRun> _allTests {
		[this] () { runTest<Tests::RedCircle>(); },
		[this] () { runTest<Tests::BlueCircle>(); },
		[this] () { runTest<Tests::RedDiamond>(); },
		[this] () { runTest<Tests::RedSquare>(); },
		[this] () { runTest<Tests::RedTriangle>(); },
		[this] () { runTest<Tests::VariousFormats>(); },
		[this] () { runTest<Tests::VariousColors>(); },
		[this] () { runTest<Tests::VariousMixed>(); },
	};

	for (auto test : _allTests) allTests.push(test);
}

void AppController::initializeDb() {
	Model::ResultDb::database();
}

int AppController::run() {
	app.setApplicationName(QString::fromUtf8(u8"Tempo de Reação"));

	infoDialog->show();

	return app.exec();
}

void AppController::runNextTest() {
	if (allTests.empty()) {
		testDialog->drawText(QString::fromUtf8(u8"Teste concluído."));
		allResults.saveTo(Model::ResultDb::database());
	}
	else {
		TestRun test = allTests.front();
		allTests.pop();

		test();
	}
}

void AppController::startTesting() {
	baseResult = infoDialog->getDataAsResult();
	testDialog = std::make_shared<TestDialog>();
	testDialog->show();

	runNextTest();
}

template <typename T>
void AppController::runTest() {
	currentTest = utils::make_unique_cast<Tests::Base, T>(testDialog);

	connect(
		currentTest.get(), SIGNAL(testCompleted(const std::vector<std::chrono::microseconds> &)),
		this, SLOT(testCompleted(const std::vector<std::chrono::microseconds>&)) );

	connect(
		currentTest.get(), SIGNAL(testCanceled()),
		testDialog.get(), SLOT(reject()) );

	connect(
		currentTest.get(), SIGNAL(testStarted(const QString&)),
		testDialog->getResultsDialog(), SLOT(startTest(const QString&)) );

	connect(
		currentTest.get(), SIGNAL(dataPointAcquired(std::chrono::microseconds)),
		testDialog->getResultsDialog(), SLOT(addResult(std::chrono::microseconds)) );

	currentTest->execute();
}

void AppController::testCompleted(const std::vector<std::chrono::microseconds>& results) {
	testDialog->drawNothing();

	Model::ResultList resultList(baseResult, currentTest->testIdentifier(), results);
	allResults += std::move(resultList);

	currentTest.release();
	runNextTest();
}
