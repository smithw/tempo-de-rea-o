#include <QtCore>
#include "AppController.h"

int main(int argc, char *argv[]) {
	AppController controller(argc, argv);

	return controller.run();
}
