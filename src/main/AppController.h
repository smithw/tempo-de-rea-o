#ifndef APPCONTROLLER_H
#define APPCONTROLLER_H

#include <QtCore>
#include <QtGui>
#include <memory>
#include <functional>
#include <queue>

#include "../ui/InfoDialog.h"
#include "../ui/TestDialog.h"
#include "../tests/Base.h"
#include "../tests/RedCircle.h"
#include "../tests/BlueCircle.h"
#include "../tests/RedSquare.h"
#include "../tests/RedDiamond.h"
#include "../tests/RedTriangle.h"
#include "../tests/VariousFormats.h"
#include "../tests/VariousColors.h"
#include "../tests/VariousMixed.h"
#include "../utils/utils.h"
#include "../model/Result.h"
#include "../model/ResultList.h"
#include "../model/ResultDb.h"

class AppController : public QObject {
	Q_OBJECT

private:
	typedef std::function<void ()> TestRun;

	QApplication app;
	std::shared_ptr<InfoDialog> infoDialog;
	std::shared_ptr<TestDialog> testDialog;
	std::unique_ptr<Tests::Base> currentTest;
	std::queue<TestRun> allTests;

	Model::Result baseResult;
	Model::ResultList allResults;

	void connectSignals();
	void generateTests();


public:
	AppController(int argc, char *argv[]);

	int run();
	void runNextTest();

	template <typename T>
	void runTest();

public slots:
	void startTesting();
	void testCompleted(const std::vector<std::chrono::microseconds>&);
	void initializeDb();

signals:
	void appStarting();
};


#endif