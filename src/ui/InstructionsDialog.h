#ifndef INSTRUCTIONSDIALOG_H
#define INSTRUCTIONSDIALOG_H

#include <QtGui>
#include <QDialog>
#include <memory>
#include "ui_instructionsDialog.h"

class InstructionsDialog : public QDialog, public Ui::InstructionsDialog {
	Q_OBJECT
public:
	InstructionsDialog(QWidget *);

	void setInstructionsText(QString&&);
};

#endif