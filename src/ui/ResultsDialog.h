#ifndef RESULTSDIALOG_H
#define RESULTSDIALOG_H

#include <QtGui>
#include <QDialog>
#include <chrono>
#include <memory>
#include "ui_resultsDialog.h"

class ResultsDialog : public QDialog, public Ui::ResultsDialog {
	Q_OBJECT
private:
	std::unique_ptr<QStringListModel> model;

	void appendRow(const QString&, bool);

public:
	ResultsDialog(QWidget *);

public slots:
	void addResult(std::chrono::microseconds);
	void startTest(const QString&);
};

#endif