#include "InfoDialog.h"
#include "../utils/utils.h"
#include "../model/ResultDb.h"

InfoDialog::InfoDialog() : QDialog(nullptr) {
	setupUi(this);
	QRegExp re("^.+$");
	validator = utils::make_unique<QRegExpValidator>(re);
	nameEntry->setValidator(validator.get());
	startButton->setEnabled(false);
	errorLabel->setVisible(false);

	connect(
		startButton, SIGNAL(clicked()),
		this, SLOT(testInput()) );

	connect(
		nameEntry, SIGNAL(textChanged(const QString&)),
		this, SLOT(validateName()) );
}

Model::Result InfoDialog::getDataAsResult() {
	Model::Result result(nameEntry->text(), ageEntry->currentIndex(), genderEntry->currentIndex());

	return result;
}

void InfoDialog::testInput() {
	auto name = nameEntry->text();
	auto db = Model::ResultDb::database();

	if (db->doesNameExist(name)) {
		nameEntry->setText("");
		nameEntry->setFocus();
		errorLabel->setText("O nome " + name + QString::fromUtf8(u8" já existe."));
		errorLabel->setVisible(true);
		validateName();
	}
	else accept();
}

void InfoDialog::validateName() {
	if (nameEntry->hasAcceptableInput()) {
		if (errorLabel->isVisible()) QTimer::singleShot(500, errorLabel, SLOT(hide()));
		startButton->setEnabled(true);
	}
	else startButton->setEnabled(false);
}