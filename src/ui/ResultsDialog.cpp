#include "ResultsDialog.h"
#include "../utils/utils.h"
#include <sstream>

ResultsDialog::ResultsDialog(QWidget * parent) : QDialog(parent) {
	setupUi(this);
	
	setWindowFlags(Qt::Tool);

	model = utils::make_unique<QStringListModel>();
	resultListView->setModel(model.get());
}

void ResultsDialog::appendRow(const QString& value, bool bold = false) {
	int count = model->rowCount();
	model->insertRows(count, 1);
	QModelIndex idx = model->index(count);
	model->setData(idx, value, Qt::DisplayRole);

	if (bold) {
		QFont bold;
		bold.setBold(true);
		model->setData(idx, bold, Qt::FontRole);
	}

	resultListView->scrollToBottom();
}

void ResultsDialog::addResult(std::chrono::microseconds result) {
	std::stringstream ss;
	ss.precision(3);
	ss << std::fixed << ((double) result.count() / 1000000.0);

	appendRow(QString::fromStdString(ss.str()) + "s");
}

void ResultsDialog::startTest(const QString& testName) {
	appendRow(testName, true);
}