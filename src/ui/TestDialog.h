#ifndef TESTDIALOG_H
#define TESTDIALOG_H

#include <QtCore>
#include <QtGui>
#include <QDialog>
#include <memory>
#include "ui_testDialog.h"
#include "ResultsDialog.h"

class TestDialog : public QDialog, public Ui::TestDialog {
	Q_OBJECT

public:
	typedef std::function<void ()> DrawFunction;

private:
	unsigned border;
	DrawFunction nextDraw;

	std::unique_ptr<QPainter> getPainter(Qt::GlobalColor color);
	ResultsDialog * resultsDialog;

public:
	static const DrawFunction drawNoop;
	TestDialog();

	ResultsDialog * getResultsDialog();

	bool eventFilter(QObject *, QEvent *);
	bool filterKey(QKeyEvent *);
	bool paintInDrawingArea(QPaintEvent * event);

	DrawFunction getNextDraw();
	void setNextDraw(DrawFunction);

	void drawCircle(Qt::GlobalColor);
	void drawText(const QString &);
	void drawSquare(Qt::GlobalColor);
	void drawTriangle(Qt::GlobalColor);
	void drawDiamond(Qt::GlobalColor);
	void drawNothing();

	QPoint getCenter();
	QPointF getCenterF();
	int getRadius();
	double getRadiusF();

signals:
	void keyPressed();
};

#endif