#include "TestDialog.h"
#include <QKeyEvent>
#include "../utils/utils.h"
#include <cmath>
#include <QPolygonF>

extern const TestDialog::DrawFunction TestDialog::drawNoop = [] () {};

TestDialog::TestDialog() : QDialog(nullptr) {
	setupUi(this);
	resultsDialog = new ResultsDialog(this); // Using Qt Parent for memory management
	resultsDialog->show();

	setNextDraw(drawNoop);

	border = 40;

	drawingArea->installEventFilter(this);
}

ResultsDialog * TestDialog::getResultsDialog() {
	return resultsDialog;
}

std::unique_ptr<QPainter> TestDialog::getPainter(Qt::GlobalColor color) {
	auto painter = utils::make_unique<QPainter>();

	painter->begin(drawingArea);
	painter->setRenderHint(QPainter::Antialiasing, true);
	painter->setPen(color);
	painter->setBrush(QBrush(color, Qt::SolidPattern));

	return painter;
}

TestDialog::DrawFunction TestDialog::getNextDraw() {
	return nextDraw;
}

void TestDialog::setNextDraw(TestDialog::DrawFunction f) {
	nextDraw = f;
}

bool TestDialog::filterKey(QKeyEvent * event) {
	if (event->key() != Qt::Key_Escape) {
		emit keyPressed();
		return true;
	}
	else return false;
}


bool TestDialog::eventFilter(QObject * obj, QEvent * event) {
	bool consumeEvent = false;

	if (event->type() == QEvent::KeyPress) {
		consumeEvent = filterKey(dynamic_cast<QKeyEvent *>(event));
	}
	else if (event->type() == QEvent::Paint) {
		consumeEvent = paintInDrawingArea(dynamic_cast<QPaintEvent *>(event));
	}

	return consumeEvent;
}

bool TestDialog::paintInDrawingArea(QPaintEvent * event) {
	getNextDraw()();

	return true;
}

void TestDialog::drawCircle(Qt::GlobalColor color) {
	setNextDraw([this, color] () {
		std::unique_ptr<QPainter> painter(getPainter(color));

		painter->drawEllipse(getCenter(), getRadius(), getRadius());
		painter->end();
	});

	drawingArea->update();
	qApp->processEvents();
}

QPoint TestDialog::getCenter() {
	return QPoint(drawingArea->width() / 2, drawingArea->height() / 2);
}

QPointF TestDialog::getCenterF() {
	return QPointF((double) drawingArea->width() / 2.0, (double) drawingArea->height() / 2.0);
}

int TestDialog::getRadius() {
	return (qMin(drawingArea->width(), drawingArea->height()) - 2*border) / 2;
}

double TestDialog::getRadiusF() {
	return(qMin((double) drawingArea->width(), (double) drawingArea->height()) - 2.0*border) / 2.0;
}

void TestDialog::drawTriangle(Qt::GlobalColor color) {
	setNextDraw([this, color] () {
		std::unique_ptr<QPainter> painter(getPainter(color));
		double r = getRadiusF();
		auto center = getCenterF();
		double halfSide = 2*r / std::sqrt(3.0);
		std::vector<QPointF> triangle {
			QPointF(center.x(), center.y() - r),
			QPointF(center.x() - halfSide, center.y() + r),
			QPointF(center.x() + halfSide, center.y() + r)
		};

		painter->drawPolygon(QPolygonF(QVector<QPointF>::fromStdVector(triangle)));
		painter->end();
	});

	drawingArea->update();
	qApp->processEvents();
}

void TestDialog::drawDiamond(Qt::GlobalColor color) {
	setNextDraw([this, color] () {
		auto center = getCenterF();
		double r = getRadiusF();
		std::unique_ptr<QPainter> painter(getPainter(color));
		std::vector<QPointF> diamond {
			QPointF(center.x() - r, center.y()),
			QPointF(center.x(), center.y() - r),
			QPointF(center.x() + r, center.y()),
			QPointF(center.x(), center.y() + r),
		};

		painter->drawPolygon(QPolygonF(QVector<QPointF>::fromStdVector(diamond)));
		painter->end();
	});

	drawingArea->update();
	qApp->processEvents();
}

void TestDialog::drawSquare(Qt::GlobalColor color) {
	setNextDraw([this, color] () {
		auto center = getCenterF();
		double r = getRadiusF();
		std::unique_ptr<QPainter> painter(getPainter(color));
		QPointF topLeft(center.x() - r, center.y() - r);
		QSizeF size(2*r, 2*r);

		painter->drawRect(QRectF(topLeft, size));
		painter->end();
	});

	drawingArea->update();
	qApp->processEvents();	
}

void TestDialog::drawText(const QString& text) {
	setNextDraw([this, text] () {
		QFont font;
		std::unique_ptr<QPainter> painter(getPainter(Qt::black));

		font.setPointSize(32);

		painter->setFont(font);
		painter->drawText(drawingArea->rect(), Qt::AlignCenter, text, nullptr);
		painter->end();
	});

	drawingArea->update();
	qApp->processEvents();
}

void TestDialog::drawNothing() {
	setNextDraw(drawNoop);
	drawingArea->update();
	qApp->processEvents();
}
