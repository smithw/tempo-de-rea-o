#ifndef INFODIALOG_H
#define INFODIALOG_H

#include <QDialog>
#include <memory>
#include "ui_infoDialog.h"
#include "../model/Result.h"

class InfoDialog : public QDialog, public Ui::InfoDialog {
	Q_OBJECT

private:
	std::unique_ptr<QRegExpValidator> validator;

public:
	Model::Result getDataAsResult();

	InfoDialog();

public slots:
	void testInput();
	void validateName();

};

#endif