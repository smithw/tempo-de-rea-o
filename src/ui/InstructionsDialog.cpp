#include "InstructionsDialog.h"

InstructionsDialog::InstructionsDialog(QWidget * parent) : QDialog(parent) {
	setupUi(this);
}

void InstructionsDialog::setInstructionsText(QString&& newText) {
	instructionLabel->setText(newText);
}
