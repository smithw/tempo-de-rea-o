#ifndef UTILS_H
#define UTILS_H

#include <memory>
#include <vector>

namespace utils {
	template<typename T, typename... Args>
	std::unique_ptr<T> make_unique(Args&&... args) {
		return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
	}

	template<typename T, typename Y, typename... Args>
	std::unique_ptr<T> make_unique_cast(Args&&... args) {
		return std::unique_ptr<T>(dynamic_cast<T*>(new Y(std::forward<Args>(args)...)));
	}

	template <typename T>
	using shared_vector = std::vector<std::shared_ptr<T>>;
}

#endif
