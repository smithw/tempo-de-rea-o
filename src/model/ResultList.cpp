#include "ResultList.h"
#include <cstdlib>
#include <sstream>

using namespace Model;

ResultList::ResultList(const Result& baseResult, const QString& testId, const std::vector<std::chrono::microseconds>& timeList) {
	for (auto resMs : timeList) {
		Result result(baseResult);
		result.setTestId(testId);
		result.setTime(resMs);

		results.push_back(result);
	}
}

void ResultList::append(ResultList&& other) {
	for (auto r : other) {
		results.insert(results.end(), std::move(r));
	}

}

void ResultList::saveTo(ResultDb * db) {
	for (auto r : *this) {
		r.saveTo(db);
	}
}

ResultList::operator QString () {
	QString str;
	QTextStream ss(&str);

	ss << "[ ";

	for (auto it = begin(); it != end(); it++) {
		ss << *it;
		if (it != end() - 1) ss << ", ";
	}

	ss << " ]";

	return (str);
}

ResultList::operator std::string () {
	std::stringstream ss;

	ss << "[ ";

	for (auto it = begin(); it != end(); it++) {
		ss << static_cast<std::string>(*it);
		if (it != end() - 1) ss << ", ";
	}

	ss << " ]";

	return (ss.str());
}