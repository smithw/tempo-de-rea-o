#ifndef RESULT_H
#define RESULT_H

#include <QtCore>
#include <chrono>
#include "ResultDb.h"


enum class AgeGroup {
	kLowerEqual18, kFrom19To22, kFrom23To27, kFrom28To35,
	kFrom46To47, kFrom48To55, kFrom56To65, kGreaterEqual66
};

enum class Gender {
	kMale, kFemale, kOther
};

namespace Model {
	class Result {
	private:
		QString personId;
		AgeGroup ageGroup;
		Gender gender;
		QString testId;
		QString formattedTime;

		QPair<QString, QVariant> makePair(QString, QString);
		QPair<QString, QVariant> makePair(QString, int);

	public:
		Result();
		Result(const QString&, int, int);
		Result(const QString&, int, int, const QString&, const QString&);
		Result(const QString&, int, int, const QString&, std::chrono::microseconds);
		Result(const Result&);
		Result(Result&&);

		Result& operator=(const Result&);
		Result& operator=(Result&&);

		void setPersonId(const QString&);
		void setPersonId(const QString&&);
		const QString& getPersonId() const;
		void setAgeGroup(const AgeGroup&);
		AgeGroup getAgeGroup() const;
		void setGender(const Gender&);
		Gender getGender() const;
		void setTestId(const QString&);
		void setTestId(const QString&&);
		const QString& getTestId() const;
		void setFormattedTime(const QString&);
		void setFormattedTime(const QString&&);
		void setTime(std::chrono::microseconds);
		const QString& getFormattedTime() const;

		operator std::string ();
		operator QString ();

		void saveTo(ResultDb *);
	};
}

#endif