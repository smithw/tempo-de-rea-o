#include "Result.h"
#include <sstream>

using namespace Model;

Result::Result() : ageGroup(AgeGroup::kLowerEqual18), gender(Gender::kOther) {
	
}

Result::Result(const QString& personId, int ageGroup, int gender) :
	personId(personId), ageGroup(static_cast<AgeGroup>(ageGroup)),
	gender(static_cast<Gender>(gender)) {

}


Result::Result(const QString& personId, int ageGroup, int gender, const QString& testId, const QString& fTime) :
	personId(personId), ageGroup(static_cast<AgeGroup>(ageGroup)),
	gender(static_cast<Gender>(gender)), testId(testId), formattedTime(fTime) {

}

Result::Result(const QString& personId, int ageGroup, int gender, const QString& testId, std::chrono::microseconds resultTime) :
	personId(personId), ageGroup(static_cast<AgeGroup>(ageGroup)),
	gender(static_cast<Gender>(gender)), testId(testId) {
	setTime(resultTime);
}


Result::Result(const Result& other) {
	personId = other.personId;
	ageGroup = other.ageGroup;
	gender = other.gender;
	testId = other.testId;
	formattedTime = other.formattedTime;
}

Result::Result(Result&& other) {
	personId = std::move(other.personId);
	ageGroup = std::move(other.ageGroup);
	gender = std::move(other.gender);
	testId = std::move(other.testId);
	formattedTime = std::move(other.formattedTime);
}

Result& Result::operator=(const Result& other) {
	personId = other.personId;
	ageGroup = other.ageGroup;
	gender = other.gender;
	testId = other.testId;
	formattedTime = other.formattedTime;

	return *this;
}

Result& Result::operator=(Result&& other) {
	personId = std::move(other.personId);
	ageGroup = std::move(other.ageGroup);
	gender = std::move(other.gender);
	testId = std::move(other.testId);
	formattedTime = std::move(other.formattedTime);
	
	return *this;
}

void Result::setPersonId(const QString& newPersonId) {
	this->personId = newPersonId;
}

void Result::setPersonId(const QString&& newPersonId) {
	this->personId = newPersonId;
}

const QString& Result::getPersonId() const {
	return this->personId;
}

void Result::setAgeGroup(const AgeGroup& newAgeGroup) {
	this->ageGroup = newAgeGroup;
}

AgeGroup Result::getAgeGroup() const {
	return this->ageGroup;
}

void Result::setGender(const Gender& newGender) {
	this->gender = newGender;
}

Gender Result::getGender() const {
	return this->gender;
}

void Result::setTestId(const QString& newTestId) {
	this->testId = newTestId;
}

void Result::setTestId(const QString&& newTestId) {
	this->testId = newTestId;
}

const QString& Result::getTestId() const {
	return this->testId;
}

void Result::setFormattedTime(const QString& newFormattedTime) {
	this->formattedTime = newFormattedTime;
}

void Result::setFormattedTime(const QString&& newFormattedTime) {
	this->formattedTime = newFormattedTime;
}

void Result::setTime(std::chrono::microseconds newTime) {
	std::stringstream ss;
	ss.precision(3);
	ss << std::fixed << ((double) newTime.count() / 1000000.0);

	formattedTime = QString::fromStdString(ss.str());
} 

const QString& Result::getFormattedTime() const {
	return this->formattedTime;
}


Result::operator std::string () {
	std::stringstream ss;

	ss << "Result { personId: " << personId.toStdString()
		<< ", ageGroup: " << static_cast<int>(ageGroup)
		<< ", gender: " << static_cast<int>(gender)
		<< ", testId: " << testId.toStdString() 
		<< ", formattedTime: " << formattedTime.toStdString()
		<< " }" ;

	return (ss.str());
}

Result::operator QString () {
	QString str;
	QTextStream ss(&str);

	ss << "Result { personId: " << personId
		<< ", ageGroup: " << static_cast<int>(ageGroup)
		<< ", gender: " << static_cast<int>(gender)
		<< ", testId: " << testId
		<< ", formattedTime: " << formattedTime
		<< " }" ;

	return (str);
	
}

QPair<QString, QVariant> Result::makePair(QString key, QString value) {
	return qMakePair(key, QVariant(value));
}

QPair<QString, QVariant> Result::makePair(QString key, int value) {
	return qMakePair(key, QVariant(value));
}

void Result::saveTo(ResultDb * db) {
	db->insertRecord(QList<QPair<QString, QVariant>>()
		<< makePair("personId", personId)
		<< makePair("ageGroup", static_cast<int>(ageGroup))
		<< makePair("gender", static_cast<int>(gender))
		<< makePair("testId", testId)
		<< makePair("formattedTime", formattedTime)
	);
}