#include "ResultDb.h"
#include "../utils/utils.h"

using namespace Model;

QSqlDatabase ResultDb::db = QSqlDatabase::addDatabase("QSQLITE");
ResultDb * ResultDb::singleton = nullptr;

ResultDb * ResultDb::database() {
	if (singleton == nullptr) {
		db.setDatabaseName(kDbFileName);
		db.open();
		singleton = new ResultDb();
	}

	return singleton;
}

ResultDb::ResultDb() {
	if (!db.tables().contains("results")) createTables();

}

ResultDb::~ResultDb() {
	db.close();
	singleton = nullptr;
}

void ResultDb::createTables() {
	QSqlQuery query(db);
	query.prepare(R"+(create table results (
		id integer primary key autoincrement,
		personId text,
		ageGroup integer,
		gender integer,
		testId text,
		formattedTime text
		);)+");

	if (!query.exec()) abort();
}

void ResultDb::insertRecord(QList<QPair<QString, QVariant>> pairs) {
	QString queryText, fields, values;
	QTextStream sQuery(&queryText), sFields(&fields), sValues(&values);
	QSqlQuery query(db);

	for (auto it = pairs.begin(); it != pairs.end(); it++) {
		sFields << it->first;
		sValues << "?";

		if (it != pairs.end() - 1) {
			sFields << ", ";
			sValues << ", ";
		}
	}

	sQuery << "insert into results (" << fields << ") values (" << values << ");";
	query.prepare(queryText);

	for (auto pair : pairs) query.addBindValue(pair.second);

	if (!query.exec()) {
		auto err = query.lastError();
		qDebug() << err.type() << err.databaseText() << err.driverText() << err.text();
		abort();
	}
}

bool ResultDb::doesNameExist(const QString& name) const {
	QString queryText("select id from results where personId = ?;");
	QSqlQuery query(db);

	query.prepare(queryText);
	query.addBindValue(QVariant(name));

	if (query.exec()) return query.next();
	else {
		auto err = query.lastError();
		qDebug() << err.type() << err.databaseText() << err.driverText() << err.text();
		abort();
		return true;
	}
}