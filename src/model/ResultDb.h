#ifndef TEMPODEREACAO_RESULT_DB_H
#define TEMPODEREACAO_RESULT_DB_H

#include <QtCore>
#include <QtSql>
#include <memory>
#include <utility>

namespace Model {
	const QString kDbFileName = "results.sqlite";

	class ResultDb {
	private:
		static QSqlDatabase db;
		static ResultDb * singleton;

		void createTables();

	public:
		static ResultDb * database();
		ResultDb();
		~ResultDb();
		
		void insertRecord(QList<QPair<QString, QVariant>>);
		bool doesNameExist(const QString&) const;
	};
}

#endif