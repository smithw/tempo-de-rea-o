#ifndef TEMPODEREACAO_RESULT_LIST_H
#define TEMPODEREACAO_RESULT_LIST_H

#include <QtCore>
#include <QtSql>
#include <vector>
#include <memory>
#include <chrono>
#include "../utils/utils.h"
#include "Result.h"
#include "ResultDb.h"

namespace Model {
	class ResultList {
	private:
		std::vector<Result> results;

	public:
		ResultList() { }
		ResultList(const Result& baseResult, const QString& testId, const std::vector<std::chrono::microseconds>&);

		void append(ResultList&&);
		ResultList& operator+=(ResultList&& other) { append(std::move(other)); return *this; }

		void saveTo(ResultDb *);

		std::vector<Result>::iterator begin() { return results.begin(); };
		std::vector<Result>::iterator end() { return results.end(); };

		operator QString ();
		operator std::string ();
	};
}

#endif