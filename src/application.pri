TEMPLATE = app
TARGET = tempo_reacao
DEPENDPATH += . main timer ui tests utils
INCLUDEPATH += . main timer ui tests utils

FORMS += ui/infoDialog.ui ui/testDialog.ui ui/instructionsDialog.ui ui/resultsDialog.ui

HEADERS += \
	timer/timer.h \
	ui/InfoDialog.h \
	ui/TestDialog.h \
	ui/InstructionsDialog.h \
	ui/ResultsDialog.h \
	main/AppController.h \
	tests/Base.h \
	tests/BaseMixed.h \
	tests/RedCircle.h \
	tests/BlueCircle.h \
	tests/RedDiamond.h \
	tests/RedSquare.h \
	tests/RedTriangle.h \
	tests/VariousFormats.h \
	tests/VariousColors.h \
	tests/VariousMixed.h \
	model/Result.h \
	model/ResultList.h \
	model/ResultDb.h \
	utils/utils.h

SOURCES += \
	timer/timer.cpp \
	ui/InfoDialog.cpp \
	ui/TestDialog.cpp \
	ui/InstructionsDialog.cpp \
	ui/ResultsDialog.cpp \
	main/main.cpp \
	main/AppController.cpp \
	tests/Base.cpp \
	tests/BaseMixed.cpp \
	tests/RedCircle.cpp \
	tests/BlueCircle.cpp \
	tests/RedTriangle.cpp \
	tests/RedDiamond.cpp \
	tests/RedSquare.cpp \
	tests/VariousFormats.cpp \
	tests/VariousColors.cpp \
	tests/VariousMixed.cpp \
	model/Result.cpp \
	model/ResultList.cpp \
	model/ResultDb.cpp

QT += sql

QMAKE_CXX=/usr/bin/g++
QMAKE_CC=/usr/bin/gcc
QMAKE_CXXFLAGS += --std=c++11

# CONFIG += debug

# DESTDIR = ../debug
# OBJECTS_DIR = ../debug/.obj
# MOC_DIR = ../debug/.moc
# RCC_DIR = ../debug/.rcc
# UI_DIR = ui

DESTDIR = ../build
OBJECTS_DIR = ../build/.obj
MOC_DIR = ../build/.moc
RCC_DIR = ../build/.rcc
UI_DIR = ui
